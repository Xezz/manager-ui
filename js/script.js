"use strict";

var app = angular.module("managerui", ["ngRoute", "ngResource"]);

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/manager-ui/teams.html',
            controller: 'TeamController'
        })
        .when('/teams', {
            templateUrl: '/manager-ui/teams.html',
            controller: 'TeamController'
        })
        .when('/spieler/:id', {
            templateUrl: '/manager-ui/spieler.html',
            controller: "SpielerController"
        })
        .when('/vereine', {
            templateUrl: '/manager-ui/vereine.html',
            controller: 'VereineController'
        })
        .when('/spieltag', {
            templateUrl: '/manager-ui/spieltag.html',
            controller: 'SpieltagController'
        })
        .when('/ergebnisse', {
            templateUrl: '/manager-ui/ergebnisse.html',
            controller: 'ErgebnisController'
        });
}]);
// FIXME: Pagination im REST request!
app.controller("TeamController", function($scope, $http) {
    $http.get("/managerapi/manager").success(function (data) {
        $scope.teams = data.resources;
    })
});
app.controller("SpielerController", function($scope, $resource) {
    $scope.spieler = $resource('/managerapi/player').get();
});

app.controller("VereineController", function($scope, $http) {
    $http.get("/managerapi/club").success(function (data) {
        $scope.vereine = data._embedded.club;
    })
});

app.controller("SpieltagController", function($scope, $http) {
    $http.get("/managerapi/result").success(function (data) {
        $scope.results = data._embedded.result;
    })
});
// TODO: Das ist ein Aggregator
app.controller("ErgebnisController", function($scope, $http) {
    $http.get("/managerapi/result").success(function (data) {
        $scope.ergebnisse = data.resources;
    })
});
