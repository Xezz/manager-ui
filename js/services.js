/**
 * Created by Xezz on 22.10.2014.
 * First replacement of the $http directive/service
 */
"use strict";

angular.module('serviceFactory', ['ngResource']).factory('playerService', ['$resource', function($resource) {
    console.log('Called serviceFactory');
    return $resource('/manager/players/:playerId', {}, {
        get: {
            method: 'GET',
            params: {
                playerId: '@id'
            }
        },
        query: {
            method: 'GET',
            isArray: true
        }
    })
}]);