/**
 * Created by Xezz on 25.10.2014.
 */
"use strict";

angular.module('controllerFactory', ['ngRoute', 'serviceFactory'])
    .controller('playerController', function($scope, playerService) {
        console.log("called pController");
        $scope.spieler = playerService.query().resources;

});
